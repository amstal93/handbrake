FROM alpine:20210212
LABEL maintainer "ToM <tom@leloop.org>"

ARG HANDBRAKE_VERSION=1.3.3-r1

WORKDIR /files
ENTRYPOINT ["HandBrakeCLI"]

RUN echo '@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    && apk --no-cache add handbrake@testing=${HANDBRAKE_VERSION}
